import '../styles/globals.css'
import NavBar from '../components/NavBar.js'
import Container from 'react-bootstrap/Container'
import 'bootstrap/dist/css/bootstrap.min.css';


export default function MyApp({ Component, pageProps }) {
  return (
  	<>
  	<NavBar/>
  	<Container>
  	<Component {...pageProps} />
  	</Container>
  	</>
  	)
}