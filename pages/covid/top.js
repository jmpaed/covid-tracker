// this page will display the countries with the most number of covid cases
import {Doughnut} from 'react-chartjs-2'
import toNum from '../../helpers/toNum.js'
import {Jumbotron} from 'react-bootstrap'


// the difference of the approach of how to fetch data and display using a single module.

// let's now build the structure of this page.

export default function Top({data}) {

	console.log(data)

	// let's get the list of countries in our collection

	const countryStatus = data.countries_stat
	// we only want to get the names of each country.
	const country = countryStatus.map(countryStat => {
		return {
			name: countryStat.country_name,
			// we have to convert the data from the cases property into numeric.
			cases: toNum(countryStat.cases)
		}
	})

	// since we are trying to evaluate the top 10 countries according to their number of cases, let's create a logic to come up with the result.
	// the goal is to sort in descending order the total number of cases.

	// syntax of sort() arr.sort([compareFunction])
	// a and b inside the argument section describes the first and second Elements.
	// the comparison between the 2 elements can lead to 3 possible scenarios.
		// 1. true (a < b)
		// 2. false(a > b)
		// equal (a = b)
	country.sort((a, b) => {
		// let's create a control strructure that will compare the cases per country according to the sequence they were fetched from the provider.
		if(a.cases < b.cases){
			return 1
		}else if(a.cases > b.cases){
			return -1
		}else{
			return 0
		}
	})

	// We still have to modify the logic above... why? because when we fetch the data from our provider, the data is actually already in ascending order.

	return(
		<>
		<Jumbotron className="top10">
			<h1 style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}>Top 10 Countries with the Highest Number of Cases.</h1>
			<Doughnut data={{
				datasets: 
				[{	
					data: [country[0].cases, country[1].cases, country[2].cases, country[3].cases, country[4].cases, country[5].cases, country[6].cases, country[7].cases, country[8].cases, country[9].cases],
					backgroundColor: ["#133337", "#bada55", "#696969", "#7fe5f0", "#ff0000", "yellow", "orange", "purple", "blue", "maroon"],
					borderColor: "#349721",
					hoverBorderColor: "#0A1E06",
				}], // how are we going to call the objects inside our array?
				// visualize [{},{},{}]
				labels: [country[0].name, country[1].name, country[2].name, country[3].name, country[4].name, country[5].name, country[6].name, country[7].name, country[8].name, country[9].name]
			}}/>
		</Jumbotron>
		</>
		)
}

// let's gather the data from our provider.

export async function getStaticProps() {
	// fetch the data from the endpoint, and make sure to indicate the correct details that describes the request.
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
	"method": "GET",
	"headers": {
		"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
		"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
		}
	})
	// let's make the response of the output readable/parasable for the browser.
	const data = await res.json()

	// return the info and pass them as props
	return {
		props: {
			data
		}
	}
}

