// first we need to identify the proper tools in order to create this new page.
import {useState} from 'react'
import {Form, Button, Alert} from 'react-bootstrap'
import Head from 'next/head'
import DoughnutChart from '../../components/DoughnutChart.js'
import toNum from '../../helpers/toNum.js'

export default function Search({data}) {
	// let's create a checker for us to be sure that the request we made was successful.
	// let's create a function that will describe the structure of our new page.
	// we are going to get 3 types of data from the covid records.
	// we would want to visualize 3 status of all covid cases which are the number of critical, recoveries, deaths.

	console.log(data)

	// let's get the status of the countries from the data variable

	const countriesStats = data.countries_stat

	const [criticals, setCriticals] = useState(0)
	const [recoveries, setRecoveries] = useState(0)
	const [deaths, setDeaths] = useState(0)
	// let's set an initial state for the country being searched by the user.

	const [name, setName] = useState("")
	const [targetCountry, setTargetCountry] = useState("")

	function search(e){
    e.preventDefault()
      const match = countriesStats.find(country => country.country_name === targetCountry)
      console.log(match)
      setName(match.country_name)
      // before we feed/inject the data inside our diagram, the info must be converted into a numerical data type first
      setCriticals(toNum(match.serious_critical))
      setDeaths(toNum(match.deaths))
      setRecoveries(toNum(match.total_recovered))
  }

	return (
	<div>
		<Head>
			<title>Covid-19 Country Search</title>
		</Head>
		<Form onSubmit={e => search(e)}>
			<Form.Group controlId="country">
				<Form.Label>Country</Form.Label>
				<Form.Control type="text" placeholder="Search for country" value={targetCountry} onChange={e => setTargetCountry(e.target.value)} />
				<Form.Text className="text-muted">Get Covid-19 stats of searched for country.
				</Form.Text>
			</Form.Group>
			<Button variant="primary" type="submit">
				Search
			</Button>
		</Form>

		{name !== '' ?
		<>
		<h1>Country: {name} </h1>
		<DoughnutChart criticals={criticals} deaths={deaths} recoveries={recoveries}/>
		</>
		:
		<Alert variant="info">Search for a country using the name</Alert>
		}

	</div>
		)
}

export async function getStaticProps() {
	//lets fetch the data from our content management service provider, make sure to properly describe the type of request with the needed credentials. lets save the response from the request inside a single variable.
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
		"method": "GET",
		"headers": {
 			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
		}
	})

	//lets make the response data from the services readable/parsable for the browser. 
	const data = await res.json()
    
    //return the data into props which we can pass/ inject inside our component.
	return {
		props: {
			data
		}
	}
}