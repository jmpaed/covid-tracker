import Head from 'next/head'
import {Jumbotron} from 'react-bootstrap'
import Banner from '../components/Banner.js'
import toNum from '../helpers/toNum.js'


// we are going to be creating a covid-19 tracking app.
// let's pass on the object property as props for us to be able to inject the data we fetched from rapid API inside our component.
export default function Home({globalTotal}) {
  return (
    <>
      <Banner />
      <Head>
        <title>Covid 19 Tracker App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Jumbotron>
      	<h1>Total Covid-19 cases in the world: <strong>{globalTotal.cases}</strong></h1>
      </Jumbotron>
    </>
  )
}

export async function getStaticProps() {
	// fetch the data from our API endpoint
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
	"method": "GET",
	"headers": {
		"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
		"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
	}
})

	//the data that will be taken from the API readable has to be readable in the browser.

	const data = await res.json()
	//after converting into json format, making it readable/parsable to get its properties
	const countriesStats = data.countries_stat // this property is provided by the API services. this property will allow us to get data which describes the total cases of COVID in all countries.

	// we are going to create a logic that will allow us to get the covid cases around the world.
	// let's declare a varialbe that will hold the total number of cases.
	let total = 0
	// let's call on the variable that holds the number of cases of each country and we are going to take the count of covid cases per country.
	countriesStats.forEach(country => {
		total += toNum(country.cases) // now the concern is the data/info gathered from rapid api is by default in a string data type, so applying a mathematical operation of any kind would not be possible. 
	})

	// let's declare an object
	const globalTotal = {
		cases: total
	}

	// let's return the object as props
	return {
		props: {
			globalTotal
		}
	}
}