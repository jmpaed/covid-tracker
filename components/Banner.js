import {Jumbotron, Button} from 'react-bootstrap';
import Link from 'next/link'

export default function Banner() {
	return(
		<Jumbotron>
			<h1>WELCOME USER!</h1>
			<p>Deaths: </p>
			<p>Recoveries: </p>
			<p>Critical Cases: </p>
			<Button variant="success">
				<Link href="/">
				<a id="bannerView">View Countries</a>
				</Link>
			</Button>
		</Jumbotron>
		)
}