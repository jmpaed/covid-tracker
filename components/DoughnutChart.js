import {Doughnut}from 'react-chartjs-2' //named export 

// let's create a function that will describe the structure of our chart that will serve to visualize the data coming from our api service.
// i will identify the props that i want to inject/pass inside our chart component
// we want to pass multiple props inside our diagram
// what are the prrops that you want to display/show/visualize inside the component. (1. data numbers(count). 2. label 3.)
// we want to be able to change/add multiple attributes to change/modify the figure of our chart. when passing multiple data sets you have to describe its properties as an object.

export default function DoughnutChart({criticals, deaths, recoveries}) {
	return(
		<Doughnut data={{
			datasets: [
				{
    			label: 'Country Stats',
    			data: [criticals, deaths, recoveries],
    			backgroundColor: [
      			"red",
      			"black",
      			"green"
    			],
    			hoverOffset: 4
  			}],
			labels: [
				'criticals',
    			'deaths',
    			'recoveries'
			]
		}} redraw={false}/>
		)
}

