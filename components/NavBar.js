import {Navbar, Nav} from 'react-bootstrap'
import Link from 'next/link'

export default function NavBar() {
	return (
		<Navbar expand="lg" id="nav">
  			<Navbar.Brand href="/">Batch 87 Covid-19 Tracker</Navbar.Brand>
  			<Navbar.Toggle aria-controls="basic-navbar-nav" />
  			<Navbar.Collapse id="basic-navbar-nav">
    			<Nav className="mr-auto">
      				{/*<Link href="/"><a className="nav-link" role="button">Infected Countries</a></Link>*/}
      				<Link href="/covid/search"><a className="nav-link" role="button">Find a Country</a></Link>
      				<Link href="/covid/top"><a className="nav-link" role="button">Top Countries</a></Link>
    			</Nav>
  			</Navbar.Collapse>
		</Navbar>
		)
}