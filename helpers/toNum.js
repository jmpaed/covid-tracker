//  function is to convert string API to result to number data type.

export default function toNum(str) {
	// convert string to array to gain access to our array methods
    const arr = [...str] // use a spread operator to acquire all string data captures from our API services.
    const filteredArr = arr.filter(element => element !== ",")
    // let's reduce the filtered array back to a single string without the commas.
    return parseInt(filteredArr.reduce((x, y) => x + y))
    }